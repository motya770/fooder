<#include "/admin/header.ftl">

<h1>Organizations</h1>

<div class="table-responsive">
    <table class="table">
        <tbody>
        <tr>
            <th>Id</th>
            <th>Name</th>
        </tr>
        <#if organizations??>
            <#list organizations as organization>
                <tr>
                    <td>${organization.id}</td>
                    <td>${organization.name}</td>
                </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

<#include "/admin/footer.ftl">


