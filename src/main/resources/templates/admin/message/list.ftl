<#include "/admin/header.ftl">

<h1>Message</h1>

<div class="table-responsive">
    <table class="table">
        <tbody>
        <tr>
            <th>Id</th>
            <th>Text</th>
            <th>Dialog Id</th>
            <th>Time</th>

            <th>Sender Id</th>
            <th>Sender Name</th>
            <th>Sender Phone</th>
        </tr>
        <#if messages??>
            <#list messages as message>
                <tr>
                    <td>${message.id!""}</td>
                    <td>${message.text!""}</td>
                    <#if message.dialog??>
                    <td>${message.dialog.id!""}</td>
                    <#else>
                    <td></td>
                    </#if>
                    <td>${message.timestampCreated!""}</td>
                     <#if message.person??>
                        <td>${message.person.id!""}</td>
                        <td>${message.person.name!""}</td>
                        <td>${message.person.phone!""}</td>
                     <#else>
                         <td></td>
                         <td></td>
                         <td></td>
                     </#if>
                </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>
<#include "/admin/footer.ftl">


