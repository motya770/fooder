<#include "/admin/header.ftl">

<h1>Orders</h1>

<div class="table-responsive">
    <table class="table">
        <tbody>
        <tr>
            <th>Id</th>
            <th>Starting Message</th>
            <th>Address</th>
            <th>Asker Id</th>
            <th>Asker Name</th>
            <th>Asker Phone</th>

            <th>Helper Id</th>
            <th>Helper Name</th>
            <th>Helper Phone</th>

            <th>Order Status</th>
            <th>Timestamp</th>
            <th>Location</th>
        </tr>
        <#if orders??>
            <#list orders as order>
                <tr>
                    <td>${order.id}</td>
                    <td>${order.startingMessage}</td>
                    <td>${order.address}</td>
                    <td>${order.asker.id}</td>
                    <td>${order.asker.name}</td>
                    <td>${order.asker.phone}</td>

                    <#if order.helper??>
                        <td>${order.helper.id!""}</td>
                        <td>${order.helper.name!""}</td>
                        <td>${order.helper.phone!""}</td>
                    <#else>
                        <td></td>
                        <td></td>
                        <td></td>
                    </#if>

                    <td>${order.orderStatus!""}</td>
                    <td>${order.timestampCreated!""}</td>
                    <td>${order.location!""}</td>
                </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

<#include "/admin/footer.ftl">


