<#include "/admin/header.ftl">

<h1>Dialog</h1>

<div class="table-responsive">
    <table class="table">
        <tbody>
        <tr>
            <th>Id</th>
            <th>Order Id</th>
            <th>Starting message</th>
            <th>Potential Helper Id</th>
            <th>Potential Helper Name</th>
            <th>Potential Helper Phone</th>
            <th>Created</th>
        </tr>
        <#if dialogs??>
            <#list dialogs as dialog>
            <tr>
                <td>${dialog.id}</td>
                <td>${dialog.order.id}</td>
                <td>${dialog.order.startingMessage}</td>

                <td>${dialog.potentialHelper.id}</td>
                <td>${dialog.potentialHelper.name}</td>
                <td>${dialog.potentialHelper.phone}</td>
                <td>${dialog.timestampCreated}</td>
            </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

<#include "/admin/footer.ftl">


