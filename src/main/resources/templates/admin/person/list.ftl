<#include "/admin/header.ftl">

<h1>Person</h1>

<div class="table-responsive">
    <table class="table">
        <tbody>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Lang</th>
            <th>Device Id</th>
            <th>Created</th>
            <th>Blocked</th>
            <th>Block</th>
        </tr>
        <#if persons??>
            <#list persons as person>
                <tr>
                    <td>${person.id!""}</td>
                    <td>${person.name!""}</td>
                    <td>${person.phone!""}</td>
                    <td>${person.language!""}</td>
                    <td>${person.deviceId!""}</td>
                    <td>${person.timestampCreated!""}</td>
                    <#if person.blocked??>
                    <td>${person.blocked?string('yes', 'no')}</td>
                    <#else>
                    <td></td>
                    </#if>
                    <td><a href="/admin/person/block?personId=${person.id}">Block</a></td>
                </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

<#include "/admin/footer.ftl">


