package com.fooder.app.web;

import com.fooder.app.model.message.Message;
import com.fooder.app.model.message.MessageDto;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.service.IMessageService;
import com.fooder.app.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/organization")
@RestController
public class OrganizationController {

    @Autowired
    private IOrganizationService organizationService;

    @PostMapping(value = "/list")
    public List<Organization> getOrganizations(){
        return organizationService.getOrganizations();
    }
}
