package com.fooder.app.web.admin;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/admin/organization")
public class AdminOrgranizationConntroller {

    @Autowired
    private OrganizationRepository organizationRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Organization> organizations = organizationRepository.findAll();
        model.addAttribute("organizations", organizations);
        return "admin/organization/list";
    }

    @RequestMapping(value = "/create-page", method = RequestMethod.GET)
    public String createPage() {
        return "/admin/organization/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam Map<String, String> body  ) {

        Organization organization = new Organization();
        organization.setName((String)body.get("name"));
        organizationRepository.save(organization);

        return "redirect:/admin/organization/list";
    }
}
