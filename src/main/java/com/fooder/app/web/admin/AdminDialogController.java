package com.fooder.app.web.admin;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.repository.DialogRepository;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.service.IDialogService;
import com.fooder.app.service.impl.DialogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/admin/dialog")
public class AdminDialogController {

    @Autowired
    private DialogRepository dialogRepository;

    @Autowired
    private IDialogService dialogService;

    private Model addAttributes(Model model){
        model.addAttribute("orderStatuses",
                Stream.of(OrderStatus.values()).collect(Collectors.toMap(OrderStatus::name, OrderStatus::name )));
        return model;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Dialog> dialogs = dialogService.getAdminDialogs();
        model.addAttribute("dialogs", dialogs);
        return "admin/dialog/list";
    }
}
