package com.fooder.app.web.admin;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/admin/order")
public class AdminOrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private IOrderService orderService;

    private  Model addAttributes(Model model){
        model.addAttribute("orderStatuses",
                Stream.of(OrderStatus.values()).collect(Collectors.toMap(OrderStatus::name, OrderStatus::name )));
        return model;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Order> orders = orderService.getAdminOrders();
        model.addAttribute("orders", orders);
        return "admin/order/list";
    }
}
