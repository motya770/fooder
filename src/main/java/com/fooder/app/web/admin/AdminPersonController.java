package com.fooder.app.web.admin;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.model.person.Person;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.PersonRepository;
import com.fooder.app.service.IPersonService;
import com.fooder.app.service.impl.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/admin/person")
public class AdminPersonController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private IPersonService personService;

    private Model addAttributes(Model model){
        model.addAttribute("orderStatuses",
                Stream.of(OrderStatus.values()).collect(Collectors.toMap(OrderStatus::name, OrderStatus::name )));
        return model;
    }

    @RequestMapping(value = "/block", method = RequestMethod.GET)
    public String block(@RequestParam Long personId, Model model) {
        personService.blockPerson(personId);
        return "redirect:/admin/person/list";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Person> persons = personService.getAdminPersons();
        model.addAttribute("persons", persons);
        return "admin/person/list";
    }
}
