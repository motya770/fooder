package com.fooder.app.web.admin;


import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.explorer.Explorer;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.person.Person;
import com.fooder.app.repository.DialogRepository;
import com.fooder.app.repository.MessageRepository;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/explorer")
public class AdminExplorerController {


    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private DialogRepository dialogRepository;

    @GetMapping(value = "/all")
    public Explorer getAll(){

        Explorer explorer = new Explorer();

        List<Message> messages = messageRepository.findAll(Sort.by("id"));
        List<Person> persons = personRepository.findAll(Sort.by("id"));
        List<Order> orders = orderRepository.findAll(Sort.by("id"));
        List<Dialog> dialogs = dialogRepository.findAll(Sort.by("id"));

        explorer.setMessages(messages);
        explorer.setOrders(orders);
        explorer.setPersons(persons);
        explorer.setDialogs(dialogs);

        return explorer;
    }

    @GetMapping(value = "/persons")
    public List<Person> getPersons(){
        return personRepository.findAll(Sort.by("id"));
    }

    @GetMapping(value = "/dialogs")
    public List<Dialog> getDialogs(){
        return dialogRepository.findAll(Sort.by("id"));
    }

    @GetMapping(value = "/orders")
    public List<Order> getOrders(){
        return orderRepository.findAll(Sort.by("id"));
    }

    @GetMapping(value = "/messages")
    public List<Message> getMessages(){
        return messageRepository.findAll(Sort.by("id"));
    }
}
