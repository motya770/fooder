package com.fooder.app.web.admin;

import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.repository.MessageRepository;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/admin/message")
public class AdminMessageController {


    @Autowired
    private IMessageService messageService;

    private Model addAttributes(Model model){
        model.addAttribute("orderStatuses",
                Stream.of(OrderStatus.values()).collect(Collectors.toMap(OrderStatus::name, OrderStatus::name )));
        return model;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Message> messages = messageService.getAdminMessages();
        model.addAttribute("messages", messages);
        return "admin/message/list";
    }
}
