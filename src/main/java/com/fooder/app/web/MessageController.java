package com.fooder.app.web;


import com.fooder.app.model.message.Message;
import com.fooder.app.model.message.MessageDto;
import com.fooder.app.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/message")
@RestController
public class MessageController {

    @Autowired
    private IMessageService messageService;

    //@RequestParam Long dialogId, @RequestParam Long personId, @RequestParam String text
    @PostMapping(value = "/send")
    public Message sendMessage(@Valid @RequestBody MessageDto messageDto){
        return messageService.createMessage(messageDto);
    }

    @PostMapping(value = "/by-order")
    public List<Message> getByOrder(@RequestParam Long orderId){
        return messageService.getMessagesByOrder(orderId);
    }

    @PostMapping(value = "/by-order-and-person")
    public List<Message> getByOrderAndPerson(@RequestParam Long orderId ){
        return messageService.getMessagesByOrder(orderId);
    }

    @PostMapping(value = "/by-dialog")
    public List<Message> getByDialog(@RequestParam Long dialogId){
        return messageService.getMessagesByDialog(dialogId);
    }
}
