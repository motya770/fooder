package com.fooder.app.web;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderDto;
import com.fooder.app.model.person.Location;
import com.fooder.app.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/order")
@RestController
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @PostMapping(value = "/create")//@RequestParam Long askerId, @RequestParam String text
    public Order createOrder(@Valid @RequestBody OrderDto order){
       return orderService.createOrder(order);
    }

    @PostMapping(value = "/edit")
    public Order editOrder(@Valid @RequestBody OrderDto order){
        return orderService.editOrder(order);
    }

    @PostMapping(value = "/by-location")//@RequestParam Long askerId, @RequestParam String text
    public List<Order> getOrdersByLocation(@RequestBody Location location){
        return orderService.getOrdersByLocation(location);
    }

    @PostMapping(value = "/asked")
    public List<Order> getOrdersWhereIAskedForHelp(@RequestParam Long askerId){
        return orderService.getOrdersWhereIAskedForHelp(askerId);
    }

    @PostMapping(value = "/helped")
    public List<Order> getOrdersWhereIHelping(@RequestParam Long helperId){
        return orderService.getOrdersWhereIHelping(helperId);
    }

    @PostMapping(value = "/start-negotiation")
    public Order createOrder(@RequestParam Long helperId, @RequestParam Long orderId){
        return orderService.startNegotiation(helperId, orderId);
    }


    @PostMapping(value = "/reject")
    public Order rejectOrder(@RequestParam Long helperId, @RequestParam Long orderId){
        return orderService.reject(helperId, orderId);
    }

    @PostMapping(value = "/accept")
    public Order acceptOrder(@RequestParam Long helperId, @RequestParam Long orderId){
        return orderService.accept(helperId, orderId);
    }

    @PostMapping(value = "/cancel")
    public Order cancelOrder(@RequestParam Long askerId, @RequestParam Long orderId){
        return orderService.cancel(askerId, orderId);
    }
}
