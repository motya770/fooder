package com.fooder.app.web;

import com.fooder.app.model.person.Person;
import com.fooder.app.model.person.PersonType;
import com.fooder.app.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/person")
@RestController
public class PersonController {

    @Autowired
    private IPersonService personService;

    @PostMapping(value = "/create")
    public Person createPerson(@RequestParam String name, @RequestParam  String phone,
                               @RequestParam(required = false) String language,
                               @RequestParam(required = false) PersonType personType,
                               @RequestParam String deviceId
    ){
       return personService.createPerson(name, phone, language, personType, deviceId);
    }

    @PostMapping(value = "/update")
    public Person updatePerson(@RequestParam Long personId, @RequestParam  String language){
        return personService.updatePerson(personId, language);
    }


}
