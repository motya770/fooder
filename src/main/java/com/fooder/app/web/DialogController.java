package com.fooder.app.web;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.service.IDialogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/dialog")
@RestController
public class DialogController {

    @Autowired
    private IDialogService dialogService;

    @PostMapping(value = "/start")
    public Dialog startDialog(@RequestParam("orderId") Long orderId, @RequestParam("potentialHelperId") Long potentialHelperId){
        return dialogService.startDialog(orderId, potentialHelperId);
    }

    @PostMapping(value = "/delete")
    public Dialog deleteDialog(@RequestParam("personId") Long personId,
                               @RequestParam("dialogId") Long dialogId){
        return dialogService.delete(personId, dialogId);
    }

    @PostMapping(value = "/by-id")
    public Dialog getById(@RequestParam("dialogId") Long dialogId){
        return dialogService.getById(dialogId);
    }

    @PostMapping(value = "/by-order")
    public List<Dialog> getDialogsForOrder(@RequestParam Long orderId){
        return dialogService.getDialogsByOrder(orderId);
    }

    @PostMapping(value = "/by-person")
    public List<Dialog> getDialogsForPerson(@RequestParam Long personId){
        return dialogService.getDialogsByPerson(personId);
    }
}
