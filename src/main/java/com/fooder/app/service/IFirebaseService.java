package com.fooder.app.service;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.person.Person;

public interface IFirebaseService {

    void sendMessage(Dialog dialog, com.fooder.app.model.message.Message message,
                     Person receiverPerson, String title, String body);

    void sendFromMyMessage(Message message);
}
