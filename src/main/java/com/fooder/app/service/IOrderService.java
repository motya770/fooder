package com.fooder.app.service;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderDto;
import com.fooder.app.model.person.Location;

import java.util.List;

public interface IOrderService {

    List<Order> getOrdersByLocation(Location location);

    List<Order> getOrdersWhereIAskedForHelp(Long askerId);

    List<Order> getOrdersWhereIHelping(Long helperId);

    Order createOrder(OrderDto orderDto);

    Order editOrder(OrderDto orderDto);

    Order startNegotiation(Long helperId, Long orderId);

    Order accept(Long helperId, Long orderId);

    Order reject(Long helperId, Long orderId);

    Order cancel(Long askerId, Long orderId);

    List<Order> getAdminOrders();
}
