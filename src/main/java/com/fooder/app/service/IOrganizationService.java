package com.fooder.app.service;

import com.fooder.app.model.organization.Organization;

import java.util.List;

public interface IOrganizationService {

    List<Organization> getOrganizations();
}
