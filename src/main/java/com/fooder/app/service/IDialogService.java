package com.fooder.app.service;

import com.fooder.app.model.dialog.Dialog;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface IDialogService {

    Dialog delete(Long personId, Long dialogId);

    Dialog getById(Long dialogId);

    List<Dialog> getDialogsByOrder(Long orderId);

    Dialog startDialog(Long orderId, Long potentialHelper);

    List<Dialog> getDialogsByPerson(Long personId);

    List<Dialog> getAdminDialogs();

}


