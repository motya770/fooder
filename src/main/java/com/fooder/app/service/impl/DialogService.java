package com.fooder.app.service.impl;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.dialog.DialogStatus;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.model.person.Person;
import com.fooder.app.repository.DialogRepository;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.PersonRepository;
import com.fooder.app.service.IDialogService;
import com.fooder.app.service.IMessageService;
import com.fooder.app.service.IPersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Transactional
@Service
public class DialogService implements IDialogService {

    @Autowired
    private DialogRepository dialogRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IPersonService personService;

    @Override
    public List<Dialog> getDialogsByPerson(Long personId) {
       Person person = personRepository.findById(personId).get();
       List<Dialog> dialogs = dialogRepository.
               findAllByDialogStatusNotAndOrder_AskerOrPotentialHelperOrderByTimestampCreated(DialogStatus.DELETED,
                       person, person);
       return dialogs;
    }

    @Override
    public List<Dialog> getAdminDialogs() {
        Person person = personService.getCurrentlyAuthenticated();
        List<Dialog> dialogs = dialogRepository.findAll();

        Organization organization = person.getOrganization();
        if(organization == null){
            return dialogs;
        }

        return dialogs.stream().filter((dialog)-> {
           return  (dialog.getPotentialHelper().getOrganization().getId().equals(organization.getId()) ||
                    dialog.getOrder().getAsker().getOrganization().getId().equals(organization.getId()));
        }).collect(Collectors.toList());
    }

    @Override
    public Dialog delete(Long personId, Long dialogId) {
        Dialog dialog = dialogRepository.findById(dialogId).get();
        if(dialog.getOrder().getAsker().getId().equals(personId) ||
            dialog.getPotentialHelper().getId().equals(personId)) {

        }else {
            log.error("Can't delete dialog {} for person id {}", dialogId, personId);
            throw new RuntimeException("Can't delete this dialog.");
        }

        dialog.setDialogStatus(DialogStatus.DELETED);
        return dialogRepository.save(dialog);
    }

    @Override
    public Dialog getById(Long dialogId) {
        return dialogRepository.findById(dialogId).get();
    }

    @Override
    public List<Dialog> getDialogsByOrder(Long orderId) {
        Order order = orderRepository.findById(orderId).get();
        return dialogRepository.findAllByOrderOrderByTimestampCreatedDesc(DialogStatus.DELETED, order);
    }

    @Override
    public Dialog startDialog(Long orderId, Long potentialHelper) {
       Order order = orderRepository.findById(orderId).get();
       Person helper = personRepository.findById(potentialHelper).get();

       Dialog dialog = dialogRepository.findAllByOrderAndPotentialHelper(order, helper, DialogStatus.DELETED);
       if(dialog!=null){
          return dialog;
       }

       dialog = new Dialog();
       dialog.setOrder(order);
       dialog.setPotentialHelper(helper);
       dialog.setDialogStatus(DialogStatus.CREATED);

       return dialogRepository.save(dialog);
    }
}
