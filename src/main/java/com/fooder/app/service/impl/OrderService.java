package com.fooder.app.service.impl;

import com.fooder.app.exception.OrderException;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderDto;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.model.person.Location;
import com.fooder.app.model.person.Person;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.PersonRepository;
import com.fooder.app.service.IOrderService;
import com.fooder.app.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class OrderService implements IOrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<Order> getOrdersByLocation(Location location) {
        return orderRepository.findByRadius(location.getLatitude(), location.getLongitude(), 5000);
    }

    @Override
    public List<Order> getOrdersWhereIAskedForHelp(Long askerId) {
        Person person = personRepository.findById(askerId).get();
        return orderRepository.findAllByAskerOrderByTimestampCreatedDesc(person);
    }

    @Override
    public List<Order> getOrdersWhereIHelping(Long helperId) {
        Person person = personRepository.findById(helperId).get();
        return orderRepository.findAllByHelperOrderByTimestampCreatedDesc(person);
    }

    @Override
    public Order editOrder(OrderDto orderDto) {
        Order order = orderRepository.findById(orderDto.getOrderId()).get();
        order.setAddress(orderDto.getAddress());
        order.setStartingMessage(orderDto.getStartingMessage());

        Location location = new Location();
        location.setLatitude(orderDto.getLatitude());
        location.setLongitude(orderDto.getLongitude());
        order.setLocation(location);

        return orderRepository.save(order);
    }

    @Override
    public Order createOrder(OrderDto orderDto) {

        Order order = new Order();

        Person asker = personRepository.findById(orderDto.getAskerId()).get();
        order.setOrderStatus(OrderStatus.CREATED);
        order.setAsker(asker);
        order.setStartingMessage(orderDto.getStartingMessage());
        order.setAddress(orderDto.getAddress());

        Location location = new Location();
        location.setLatitude(orderDto.getLatitude());
        location.setLongitude(orderDto.getLongitude());
        order.setLocation(location);

        return orderRepository.save(order);
    }

    @Override
    public Order startNegotiation(Long helperId, Long orderId) {
        return null;
    }

    @Autowired
    private IPersonService personService;

    @Override
    public List<Order> getAdminOrders() {
        Person person = personService.getCurrentlyAuthenticated();
        List<Order> orders = orderRepository.findAll();

        Organization organization = person.getOrganization();
        if(organization == null){
            return orders;
        }

        return orders.stream().filter((order)->
            order.getAsker().getOrganization().getId().equals(order.getId())
           ).collect(Collectors.toList());
    }

    @Override
    public Order cancel(Long askerId, Long orderId) {
        Order order = orderRepository.findById(orderId).get();
        if(!(order.getAsker().getId().equals(askerId))){
            throw new OrderException("AskerId: " + askerId + " is not equals to asker id in DB.");
        }

        order.setOrderStatus(OrderStatus.CANCELED);
        return orderRepository.save(order);
    }

    @Override
    public Order accept(Long helperId, Long orderId) {
        Order order = orderRepository.findById(orderId).get();

        if(!(order.getOrderStatus().equals(OrderStatus.CREATED) ||
                order.getOrderStatus().equals(OrderStatus.REJECTED))){

            throw new OrderException("This order " + order.getId() + "  is in other status " + order.getOrderStatus());
        }

        Person helper = personRepository.findById(helperId).get();
        order.setHelper(helper);
        order.setOrderStatus(OrderStatus.ACCEPTED);
        return orderRepository.save(order);
    }


    @Override
    public Order reject(Long helperId, Long orderId) {
        Order order = orderRepository.findById(orderId).get();

        if(!order.getOrderStatus().equals(OrderStatus.ACCEPTED)){
            throw new OrderException("This order " + order.getId() + "  is in other status " + order.getOrderStatus());
        }

        Person helper = personRepository.findById(helperId).get();
        if(!order.getHelper().getId().equals(helper.getId())){
            throw new OrderException("This order " + order.getId() + "  can't be rejected because you are not helper for this order. ");
        }

        order.setOrderStatus(OrderStatus.REJECTED);
        return orderRepository.save(order);
    }
}
