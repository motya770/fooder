package com.fooder.app.service.impl;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.person.Person;
import com.fooder.app.service.IFirebaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class FirebaseService implements IFirebaseService {

    @Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;

    private FirebaseApp firebaseApp;

    @PostConstruct
    public void init(){
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream()))
                    .setDatabaseUrl("https://fooder-5fb8a.firebaseio.com").build();

            firebaseApp = FirebaseApp.initializeApp(options);
        }catch (Exception e){
            log.error("{}", e);
        }
    }


    @Override
    public void sendFromMyMessage(com.fooder.app.model.message.Message message) {

        //log.info("Staring to prepare message {}", message);

        Dialog dialog = message.getDialog();

        Person receiverPerson = message.getPerson().getId().equals(dialog.getPotentialHelper().getId()) ?
                  dialog.getOrder().getAsker() : dialog.getPotentialHelper();

        log.info("Message: " + message.getId()  + " messagePerson "
                + message.getPerson().getId() + " potentialHelper: " +  dialog.getId() +
                " askerId: " + dialog.getOrder().getAsker().getId() +  " recieverId " + receiverPerson.getId());

        String title = "LaFood: " + message.getPerson().getName();
        String body = message.getText();
        sendMessage(dialog, message, receiverPerson, title, body);
    }

    @Override
    public void sendMessage(Dialog dialog, com.fooder.app.model.message.Message message, Person receiverPerson, String title, String body) {
        try{

        FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance();
        Map<String, String> data = new HashMap<>();
            data.put("click_action", "FLUTTER_NOTIFICATION_CLICK");
            data.put("title", title);
            data.put("body", body);
            data.put("messageId", message.getId().toString());
            data.put("dialogId", dialog.getId().toString());
            data.put("receiverPersonId", receiverPerson.getId().toString());
            data.put("personId", message.getPerson().getId().toString());
            data.put("timestampCreated", message.getTimestampCreated().getTime() + "");

            Message firebaseMessage = Message.builder()
                .putAllData(data)
                .setNotification(Notification.
                        builder().setBody(body)
                        .setTitle(title).build())
                .setTopic("topic_" + receiverPerson.getId())
                //.setToken("cSntR1MnTIe2AKh2KEL3t3:APA91bGcosEzaada2eTByg4vizGgs1SKqRU4E2T8AKCd3ZaxhddmvUUbohECRNTYVSXYiXMgSUfpWXka2KXMThOWWBdkb5VF1U_o3FJHkG9MgRpITbKo0JEGMBoBOiYjFaj6hUYUf-06")
               .build();
        firebaseMessaging.sendAsync(firebaseMessage);

        log.info("Sent message to: {} dialog: {} title: {} body: {}", receiverPerson.getId() + " " + receiverPerson.getName(), dialog.getId(), title, body);
        //log.info("resp: {}", resp);

        }catch (Exception e){
            log.error("{}", e);
        }
    }
}
