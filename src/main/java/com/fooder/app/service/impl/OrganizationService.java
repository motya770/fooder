package com.fooder.app.service.impl;

import com.fooder.app.model.organization.Organization;
import com.fooder.app.repository.OrganizationRepository;
import com.fooder.app.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationService implements IOrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    @Override
    public List<Organization> getOrganizations() {
        return organizationRepository.findAll();
    }
}
