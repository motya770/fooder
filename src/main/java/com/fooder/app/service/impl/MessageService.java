package com.fooder.app.service.impl;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.message.MessageDto;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.model.person.Person;
import com.fooder.app.repository.DialogRepository;
import com.fooder.app.repository.MessageRepository;
import com.fooder.app.repository.OrderRepository;
import com.fooder.app.repository.PersonRepository;
import com.fooder.app.service.IFirebaseService;
import com.fooder.app.service.IMessageService;
import com.fooder.app.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class MessageService implements IMessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DialogRepository dialogRepository;

    @Autowired
    private IFirebaseService firebaseService;

    @Autowired
    private IPersonService personService;

    @Override
    public List<Message> getAdminMessages() {
        Person person = personService.getCurrentlyAuthenticated();
        List<Message> messages = messageRepository.findAll();

        Organization organization = person.getOrganization();
        if(organization == null){
            return messages;
        }

        return messages.stream().filter((message)-> {
            return  (message.getPerson().getOrganization().getId().equals(organization.getId()))
                    ||
                    (message.getDialog().getPotentialHelper().getOrganization().getId().equals(organization.getId()) ||
                            message.getDialog().getOrder().getAsker().getOrganization().getId().equals(organization.getId()));
        }).collect(Collectors.toList());
    }

    @Override
    public List<Message> getMessagesByDialog(Long dialogId) {
        Dialog dialog = dialogRepository.findById(dialogId).get();
        List<Message> messages = messageRepository.findAllByDialogOrderByTimestampCreatedDesc(dialog);
        return messages;
    }

    @Override
    public List<Message> getMessagesByOrder(Long orderId) {
        Order order = orderRepository.getOne(orderId);
        List<Message> messages =  new ArrayList<>(); //messageRepository.findAllByOrderOrderByTimestampCreatedDesc(order);
        return messages;
    }

    @Override
    public Message createMessage(MessageDto messageDto) {

        Long dialogId = messageDto.getDialogId();
        Long personId = messageDto.getPersonId();
        String text = messageDto.getText();

        Dialog dialog = dialogRepository.findById(dialogId).get();
        Person person = personRepository.findById(personId).get();

        Message message = new Message();

        message.setDialog(dialog);
        message.setPerson(person);
        message.setText(text);

        message = messageRepository.save(message);

        firebaseService.sendFromMyMessage(message);

        return message;
    }
}
