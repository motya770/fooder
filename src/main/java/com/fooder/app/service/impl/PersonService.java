package com.fooder.app.service.impl;

import com.fooder.app.exception.PersonException;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.organization.Organization;
import com.fooder.app.model.person.Person;
import com.fooder.app.model.person.PersonType;
import com.fooder.app.repository.PersonRepository;
import com.fooder.app.service.IPersonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Transactional
@Service
public class PersonService implements IPersonService, UserDetailsService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       return personRepository.findByName(username);
    }

    @Override
    public Person blockPerson(Long personId) {
        Person person = personRepository.findById(personId).get();
        person.setBlocked(true);
        return personRepository.save(person);
    }

    @Override
    public Person createPerson(String name, String phone, String language, PersonType personType, String deviceId) {

        name = name.trim();
        phone = phone.trim();
        deviceId = deviceId.trim();
        Person person = personRepository.findByNameAndPhone(name, phone);
        if(person!=null) {
            if(!person.getDeviceId().equals(deviceId)){
                log.error("Parameter " + deviceId  + " is not equals for:  "
                        + person.getDeviceId() + " for " + name  +", id: " + person.getId());
                throw new PersonException("For person " + name + " deviceId is not suitable " + deviceId);
            }
            return person;
        }

        person = new Person();
        person.setName(name);
        person.setPhone(phone);
        person.setPersonType(personType);
        person.setLanguage(language);
        person.setDeviceId(deviceId);

        // Creates a 64 chars length of random alphabetic string.
        String randomPassword = RandomStringUtils.randomAlphabetic(7);
        String encodedRandomPassword = passwordEncoder.encode(randomPassword);
        person.setPassword(encodedRandomPassword);

        log.info("Encoding: |{}|, |{}|", name, randomPassword);

        return personRepository.save(person);
    }

    @Override
    public Person updatePerson(Long personId, String language) {
        Person person = personRepository.findById(personId).get();
        person.setLanguage(language);
        return personRepository.save(person);
    }

    @Override
    public List<Person> getAdminPersons() {
        Person person = getCurrentlyAuthenticated();
        List<Person> persons = personRepository.findAll();

        Organization organization = person.getOrganization();
        if(organization == null){
            return persons;
        }

        return persons.stream().filter((itemPerson)->
            itemPerson.getOrganization().getId().equals(organization.getId())
        ).collect(Collectors.toList());
    }

    @Override
    public Person getCurrentlyAuthenticated() {
        return (Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
