package com.fooder.app.service;

import com.fooder.app.model.person.Person;
import com.fooder.app.model.person.PersonType;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


public interface IPersonService {

    Person blockPerson(Long personId);

    Person createPerson(String name, String phone, String language, PersonType personType, String deviceId);

    Person updatePerson(Long personId, String language);

    Person getCurrentlyAuthenticated();

    List<Person> getAdminPersons();
}
