package com.fooder.app.service;

import com.fooder.app.model.message.Message;
import com.fooder.app.model.message.MessageDto;

import java.util.List;

public interface IMessageService {

    Message createMessage(MessageDto messageDto);

    List<Message> getMessagesByOrder(Long orderId);

    List<Message> getMessagesByDialog(Long dialogId);

    List<Message> getAdminMessages();
}
