package com.fooder.app.repository;

import com.fooder.app.model.organization.Organization;
import com.fooder.app.model.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

}

