package com.fooder.app.repository;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.dialog.DialogStatus;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DialogRepository extends JpaRepository<Dialog, Long> {

    @Query("select d from Dialog d where d.order = :order and " +
            " dialogStatus is not :dialogStatus " +
            "  order by d.timestampCreated desc")
    List<Dialog> findAllByOrderOrderByTimestampCreatedDesc(@Param("dialogStatus") DialogStatus dialogStatus,
                                                           @Param("order") Order order);

    @Query("select d from Dialog d where d.order = :order and " +
            " d.potentialHelper = :potentialHelper and dialogStatus is not :dialogStatus " +
            "  order by d.timestampCreated desc")
    Dialog findAllByOrderAndPotentialHelper(@Param("order") Order order,
                                            @Param("potentialHelper") Person potentialHelper,
                                            @Param("dialogStatus") DialogStatus dialogStatus);

    @Query("select d from Dialog d where d.order.asker = :asker or" +
            " d.potentialHelper = :potentialHelper and dialogStatus is not :dialogStatus " +
            "  order by d.timestampCreated desc")
    List<Dialog> findAllByDialogStatusNotAndOrder_AskerOrPotentialHelperOrderByTimestampCreated(
            @Param("dialogStatus") DialogStatus dialogStatus,
            @Param("asker") Person asker,
            @Param("potentialHelper") Person potentialHelper);
}