package com.fooder.app.repository;

import com.fooder.app.model.order.Order;
import com.fooder.app.model.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "SELECT * FROM fooder.order as o " +
            "WHERE earth_distance( ll_to_earth(o.latitude, o.longitude), ll_to_earth(:lat, :lon)) < :radius" +
            " AND (o.order_status != 'CANCELED' OR o.order_status != 'ACCEPTED')",
            nativeQuery = true)
    List<Order> findByRadius(@Param(value = "lat") double lat, @Param(value = "lon") double lon,  @Param(value = "radius") double radius);

    List<Order> findAllByAskerOrderByTimestampCreatedDesc(Person asker);

    List<Order> findAllByHelperOrderByTimestampCreatedDesc(Person helper);
}
