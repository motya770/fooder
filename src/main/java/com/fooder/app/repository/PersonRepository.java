package com.fooder.app.repository;

import com.fooder.app.model.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByNameAndPhone(String name, String phone);

    Person findByName(String name);


}
