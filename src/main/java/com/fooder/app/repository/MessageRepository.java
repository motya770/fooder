package com.fooder.app.repository;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByDialogOrderByTimestampCreatedDesc(Dialog dialog);

   // List<Message> findAllByOrderOrderByTimestampCreatedDesc(Order order);
}
