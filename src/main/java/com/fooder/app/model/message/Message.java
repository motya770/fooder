package com.fooder.app.model.message;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.person.Person;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;


@ToString
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Entity
public class Message {

    @GeneratedValue
    @Id
    private Long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Dialog dialog;

    @ManyToOne(fetch = FetchType.LAZY)
    private Person person;

    private String text;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @CreationTimestamp
    private Timestamp timestampCreated = new Timestamp(System.currentTimeMillis());
}
