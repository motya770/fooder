package com.fooder.app.model.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
public class MessageDto {

    @NotNull
    private Long dialogId;

    @NonNull
    private Long personId;

    @NotEmpty
    private String text;
}
