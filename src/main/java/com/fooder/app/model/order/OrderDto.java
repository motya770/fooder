package com.fooder.app.model.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.person.Location;
import com.fooder.app.model.person.Person;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
public class OrderDto {

   private Long orderId;

   @NotNull
   private Long askerId;

   @NotEmpty
   private String startingMessage;

   @NotEmpty
   private String address;

   @NotNull
   private Double latitude;

   @NotNull
   private Double longitude;
}
