package com.fooder.app.model.order;

public enum OrderStatus {
    CREATED, ACCEPTED, REJECTED, CANCELED, EXECUTED
}
