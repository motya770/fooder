package com.fooder.app.model.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.person.Location;
import com.fooder.app.model.person.Person;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Entity
public class Order {

    @GeneratedValue
    @Id
    private Long id;

    @NotNull
    private String startingMessage;

    @NotNull
    private String address;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Person asker;

    @ManyToOne(fetch = FetchType.LAZY)
    private Person helper;

    @NotNull
    @Enumerated( value = EnumType.STRING)
    private OrderStatus orderStatus;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private List<Dialog> dialogs;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @CreationTimestamp
    private Timestamp timestampCreated;

    @Embedded
    private Location location;
}
