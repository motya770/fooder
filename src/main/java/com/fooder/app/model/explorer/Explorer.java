package com.fooder.app.model.explorer;

import com.fooder.app.model.dialog.Dialog;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.person.Person;
import lombok.Data;

import java.util.List;

@Data
public class Explorer {

    private List<Person> persons;
    private List<Order> orders;
    private List<Message> messages;
    private List<Dialog> dialogs;

}
