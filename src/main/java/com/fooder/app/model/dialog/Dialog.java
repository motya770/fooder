package com.fooder.app.model.dialog;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fooder.app.model.message.Message;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.order.OrderStatus;
import com.fooder.app.model.person.Person;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;


@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Entity
public class Dialog {

    @GeneratedValue
    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    private Person potentialHelper;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dialog")
    private List<Message> messages;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @CreationTimestamp
    private Timestamp timestampCreated;

    @NotNull
    @Enumerated( value = EnumType.STRING)
    private DialogStatus dialogStatus;
}
