package com.fooder.app.model.dialog;

public enum  DialogStatus {
    CREATED, DELETED
}
