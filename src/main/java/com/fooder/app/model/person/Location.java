package com.fooder.app.model.person;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Location {

    private Double latitude;

    private Double longitude;
}
