package com.fooder.app.model.person;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fooder.app.model.order.Order;
import com.fooder.app.model.organization.Organization;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Table(
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "phone"})
)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Entity
public class Person implements UserDetails {

    @GeneratedValue
    @Id
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String phone;

    @JsonIgnore
    private String password;

    @NotEmpty
    private String language;

    @NotEmpty
    private String deviceId;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "asker")
    private List<Order> askerOrders;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "helper")
    private List<Order> helperOrders;

    @Enumerated(EnumType.STRING)
    private PersonType personType;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @CreationTimestamp
    private Timestamp timestampCreated;

    private Boolean blocked;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Organization organization;

    @JsonIgnore
    private String role = "ROLE_USER";

    @Override
    public String toString(){
        return "{id: " + id  + " " + name + "}";
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<SimpleGrantedAuthority> set = new HashSet<>();
        set.add( new SimpleGrantedAuthority(role));
        return set;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
