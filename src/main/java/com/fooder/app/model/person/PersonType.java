package com.fooder.app.model.person;

public enum PersonType {
    I_WANT_TO_HELP, I_NEED_HELP
}
