package com.fooder.app.model.organization;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Table(
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name"})
)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Entity
public class Organization {

    @GeneratedValue
    @Id
    private Long id;

    @NotEmpty
    private String name;
}

